unit unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Spin,
  StdCtrls, ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    labelrest: TLabel;
    eier: TTimer;
    flash: TTimer;
    timerpause: TButton;
    timerstart: TButton;
    timerstop: TButton;
    label_sekunde: TLabel;
    label_minute: TLabel;
    label_stunde: TLabel;
    hours: TSpinEdit;
    minutes: TSpinEdit;
    seconds: TSpinEdit;
    procedure eierTimer(Sender: TObject);
    procedure flashTimer(Sender: TObject);
    procedure timerpauseClick(Sender: TObject);
    procedure timerstartClick(Sender: TObject);
    procedure timerstopClick(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;
  sumsec: integer;
  timerrest: array[0..2] of integer;
  timerreststr: array[0..2] of string;
  pause: boolean;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.timerstartClick(Sender: TObject);
begin
  form1.bordericons := [];
  labelrest.font.color := clblack;
  hours.enabled := false;
  minutes.enabled := false;
  seconds.enabled := false;
  timerstart.enabled := false;
  timerstop.enabled := true;
  timerpause.enabled := true;
  sumsec := hours.value*3600 + minutes.value*60 + seconds.value;
  inc(sumsec);
  eier.interval := 1;
  eier.enabled := true;
  pause := false;
end;

procedure TForm1.timerstopClick(Sender: TObject);
begin
  form1.bordericons := [biSystemMenu];
  eier.enabled := false;
  sumsec := 1;
  hours.enabled := true;
  minutes.enabled := true;
  seconds.enabled := true;
  timerstart.enabled := true;
  timerstop.enabled := false;
  timerpause.enabled := false;
  pause := false;
  labelrest.visible := false;
  flash.enabled := false;
end;

procedure TForm1.eierTimer(Sender: TObject);
begin
  if sumsec=0 then begin
    labelrest.font.color := clred;
    flash.enabled := true;
    timerpause.enabled := false;
    beep;
  end else if pause=false then begin
    dec(sumsec);
    timerrest[0] := sumsec div 3600;
    if timerrest[0] < 10 then timerreststr[0] := '0'+inttostr(timerrest[0]) else timerreststr[0] := inttostr(timerrest[0]);
    timerrest[1] := (sumsec mod 3600) div 60;
    if timerrest[1] < 10 then timerreststr[1] := '0'+inttostr(timerrest[1]) else timerreststr[1] := inttostr(timerrest[1]);
    timerrest[2] := (sumsec mod 3600) mod 60;
    if timerrest[2] < 10 then timerreststr[2] := '0'+inttostr(timerrest[2]) else timerreststr[2] := inttostr(timerrest[2]);
    labelrest.caption := timerreststr[0] + ':' + timerreststr[1] + ':' + timerreststr[2];
    labelrest.visible := true;
    eier.interval := 1000;
  end;
end;

procedure TForm1.flashTimer(Sender: TObject);
begin
  if labelrest.visible=false then labelrest.visible := true else labelrest.visible := false;
end;

procedure TForm1.timerpauseClick(Sender: TObject);
begin
  if timerstop.enabled=true then timerstop.enabled := false else timerstop.enabled := true;
  if form1.bordericons=[] then form1.bordericons:=[biSystemMenu] else form1.bordericons:=[];
  if pause=true then begin
    flash.enabled := false;
    pause := false;
    labelrest.visible := true;
  end else begin
    pause := true;
    flash.enabled := true;
  end;
end;

end.

